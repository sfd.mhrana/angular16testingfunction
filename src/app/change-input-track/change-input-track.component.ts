import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormGroup, FormBuilder, ReactiveFormsModule, FormsModule } from '@angular/forms';


@Component({
  selector: 'app-change-input-track',
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  templateUrl: './change-input-track.component.html',
  styleUrls: ['./change-input-track.component.css']
})
export class ChangeInputTrackComponent {
  dataForm: FormGroup;
  yourData: any[] = [
    {
      stylePartId: 114,
      stylePartName: "TOP",
      subStyleId: 335,
      bomId: 0,
      subStyleName: "FORMERTESTKA",
      subStyleColor: null,
      itemTypeGroup: "CM",
      itemTypeName: "CPM",
      itemCode: '',
      itemName: '',
      consumptionQuantity: 0,
      fabricWidth: 0,
      supplierId: 0,
      supplierName: '',
      measureUnitId: 0,
      measureUnitName: '',
      unitPrice: 0,
      wastagePercentage: 0,
      comments: '',
      price: 3.32,
      id: 1,
      itemTypeId: 1
    },
    {
      stylePartId: 114,
      stylePartName: "TOP",
      subStyleId: 335,
      bomId: 0,
      subStyleName: "FORMERTESTKA",
      subStyleColor: null,
      itemTypeGroup: "CM",
      itemTypeName: "SMV",
      itemCode: '',
      itemName: '',
      consumptionQuantity: 0,
      fabricWidth: 0,
      supplierId: 0,
      supplierName: '',
      measureUnitId: 0,
      measureUnitName: '',
      unitPrice: 0,
      wastagePercentage: 0,
      comments: '',
      price: 0,
      id: 2,
      itemTypeId: 1
    },
    {
      stylePartId: 114,
      stylePartName: "TOP",
      subStyleId: 335,
      bomId: 0,
      subStyleName: "FORMERTESTKA",
      subStyleColor: null,
      itemTypeGroup: "CM",
      itemTypeName: "Efficiency",
      itemCode: '',
      itemName: '',
      consumptionQuantity: 0,
      fabricWidth: 0,
      supplierId: 0,
      supplierName: '',
      measureUnitId: 0,
      measureUnitName: '',
      unitPrice: 0,
      wastagePercentage: 0,
      comments: '',
      price: 0,
      id: 3,
      itemTypeId: 1
    },
    {
      stylePartId: 114,
      stylePartName: "TOP",
      subStyleId: 335,
      bomId: 0,
      subStyleName: "FORMERTESTKA",
      subStyleColor: null,
      itemTypeGroup: "CM",
      itemTypeName: "CM",
      itemCode: '',
      itemName: '',
      consumptionQuantity: 0,
      fabricWidth: 0,
      supplierId: 0,
      supplierName: '',
      measureUnitId: 0,
      measureUnitName: '',
      unitPrice: 0,
      wastagePercentage: 0,
      comments: '',
      price: 0,
      id: 4,
      itemTypeId: 1
    },
    {
      stylePartId: 114,
      stylePartName: "TOP",
      subStyleId: 335,
      bomId: 0,
      subStyleName: "FORMERTESTKA",
      subStyleColor: null,
      itemTypeGroup: "CM",
      itemTypeName: "CM2",
      itemCode: '',
      itemName: '',
      consumptionQuantity: 0,
      fabricWidth: 0,
      supplierId: 0,
      supplierName: '',
      measureUnitId: 0,
      measureUnitName: '',
      unitPrice: 0,
      wastagePercentage: 0,
      comments: '',
      price: 0,
      id: 5,
      itemTypeId: 1
    },
    {
      stylePartId: 115,
      stylePartName: "BUTTOM",
      subStyleId: 337,
      bomId: 0,
      subStyleName: "FORMERTESTKA",
      subStyleColor: null,
      itemTypeGroup: "CM",
      itemTypeName: "CPM",
      itemCode: '',
      itemName: '',
      consumptionQuantity: 0,
      fabricWidth: 0,
      supplierId: 0,
      supplierName: '',
      measureUnitId: 0,
      measureUnitName: '',
      unitPrice: 0,
      wastagePercentage: 0,
      comments: '',
      price: 3.32,
      id: 6,
      itemTypeId: 25
    },
    {
      stylePartId: 115,
      stylePartName: "BUTTOM",
      subStyleId: 337,
      bomId: 0,
      subStyleName: "FORMERTESTKA",
      subStyleColor: null,
      itemTypeGroup: "CM",
      itemTypeName: "SMV",
      itemCode: '',
      itemName: '',
      consumptionQuantity: 0,
      fabricWidth: 0,
      supplierId: 0,
      supplierName: '',
      measureUnitId: 0,
      measureUnitName: '',
      unitPrice: 0,
      wastagePercentage: 0,
      comments: '',
      price: 0,
      id: 7,
      itemTypeId: 25
    },
    {
      stylePartId: 115,
      stylePartName: "BUTTOM",
      subStyleId: 337,
      bomId: 0,
      subStyleName: "FORMERTESTKA",
      subStyleColor: null,
      itemTypeGroup: "CM",
      itemTypeName: "Efficiency",
      itemCode: '',
      itemName: '',
      consumptionQuantity: 0,
      fabricWidth: 0,
      supplierId: 0,
      supplierName: '',
      measureUnitId: 0,
      measureUnitName: '',
      unitPrice: 0,
      wastagePercentage: 0,
      comments: '',
      price: 0,
      id: 8,
      itemTypeId: 25
    },
    {
      stylePartId: 115,
      stylePartName: "BUTTOM",
      subStyleId: 337,
      bomId: 0,
      subStyleName: "FORMERTESTKA",
      subStyleColor: null,
      itemTypeGroup: "CM",
      itemTypeName: "CM",
      itemCode: '',
      itemName: '',
      consumptionQuantity: 0,
      fabricWidth: 0,
      supplierId: 0,
      supplierName: '',
      measureUnitId: 0,
      measureUnitName: '',
      unitPrice: 0,
      wastagePercentage: 0,
      comments: '',
      price: 0,
      id: 9,
      itemTypeId: 25
    },
    {
      stylePartId: 115,
      stylePartName: "BUTTOM",
      subStyleId: 337,
      bomId: 0,
      subStyleName: "FORMERTESTKA",
      subStyleColor: null,
      itemTypeGroup: "CM",
      itemTypeName: "CM2",
      itemCode: '',
      itemName: '',
      consumptionQuantity: 0,
      fabricWidth: 0,
      supplierId: 0,
      supplierName: '',
      measureUnitId: 0,
      measureUnitName: '',
      unitPrice: 0,
      wastagePercentage: 0,
      comments: '',
      price: 0,
      id: 10,
      itemTypeId: 25
    },
    {
      id: 11,
      stylePartId: 114,
      subStyleId: 335,
      itemTypeGroup: "Fabrics",
      itemTypeId: 1,
      itemTypeName: "BODY FABRIC",
      supplierId: 0,
      bomId: 1415,
      itemId: 6,
      consumptionQuantity: 2,
      unitPrice: 50,
      wastagePercentage: 0,
      comments: null,
      price: 100,
      serial: 'f1',
      typeSerial: 1
    },
    {
      id: 18,
      stylePartId: 114,
      subStyleId: 335,
      itemTypeGroup: "Trims",
      itemTypeId: 17,
      itemTypeName: "MAIN LABEL/SIZE LABEL",
      supplierId: 0,
      bomId: 1420,
      itemId: 15,
      consumptionQuantity: 2,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: 't4',
      typeSerial: 1
    },
    {
      id: 16,
      stylePartId: 114,
      subStyleId: 335,
      itemTypeGroup: "Trims",
      itemTypeId: 15,
      itemTypeName: "TAG PIN",
      supplierId: 0,
      bomId: 1418,
      itemId: 13,
      consumptionQuantity: 2,
      unitPrice: 50,
      wastagePercentage: 0,
      comments: null,
      price: 100,
      serial: 't2',
      typeSerial: 1
    },
    {
      id: 19,
      stylePartId: 114,
      subStyleId: 335,
      itemTypeGroup: "Trims",
      itemTypeId: 18,
      itemTypeName: "CARE LABEL",
      supplierId: 0,
      bomId: 1421,
      itemId: 9,
      consumptionQuantity: 1,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: 't5',
      typeSerial: 1
    },
    {
      id: 17,
      stylePartId: 114,
      subStyleId: 335,
      itemTypeGroup: "Trims",
      itemTypeId: 16,
      itemTypeName: "BARCODE STICKER",
      supplierId: 0,
      bomId: 1419,
      itemId: 8,
      consumptionQuantity: 1,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: 't3',
      typeSerial: 1
    },
    {
      id: 13,
      stylePartId: 114,
      subStyleId: 335,
      itemTypeGroup: "Accessories",
      itemTypeId: 19,
      itemTypeName: "SEWING THREAD",
      supplierId: 0,
      bomId: 1416,
      itemId: 5,
      consumptionQuantity: 2,
      unitPrice: 50,
      wastagePercentage: 0,
      comments: null,
      price: 100,
      serial: 'a1',
      typeSerial: 1
    },
    {
      id: 15,
      stylePartId: 114,
      subStyleId: 335,
      itemTypeGroup: "Trims",
      itemTypeId: 14,
      itemTypeName: "HANG TAG",
      supplierId: 0,
      bomId: 1417,
      itemId: 6,
      consumptionQuantity: 1,
      unitPrice: 50,
      wastagePercentage: 0,
      comments: null,
      price: 50,
      serial: 't1',
      typeSerial: 1
    },
    {
      id: 12,
      stylePartId: 115,
      subStyleId: 337,
      itemTypeGroup: "Fabrics",
      itemTypeId: 2,
      itemTypeName: "CONTRAST FABRIC",
      supplierId: 0,
      bomId: 1481,
      itemId: 7,
      consumptionQuantity: 0,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: 'f3',
      typeSerial: 1
    },
    {
      id: 24,
      stylePartId: 115,
      subStyleId: 337,
      itemTypeGroup: "Trims",
      itemTypeId: 18,
      itemTypeName: "CARE LABEL",
      supplierId: 0,
      bomId: 1407,
      itemId: 14,
      consumptionQuantity: 1,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: 't5',
      typeSerial: 1
    },
    {
      id: 23,
      stylePartId: 115,
      subStyleId: 337,
      itemTypeGroup: "Trims",
      itemTypeId: 17,
      itemTypeName: "MAIN LABEL/SIZE LABEL",
      supplierId: 0,
      bomId: 1406,
      itemId: 13,
      consumptionQuantity: 5,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: 't4',
      typeSerial: 1
    },
    {
      id: 22,
      stylePartId: 115,
      subStyleId: 337,
      itemTypeGroup: "Trims",
      itemTypeId: 16,
      itemTypeName: "BARCODE STICKER",
      supplierId: 0,
      bomId: 1405,
      itemId: 9,
      consumptionQuantity: 2,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: 't3',
      typeSerial: 1
    },
    {
      id: 21,
      stylePartId: 115,
      subStyleId: 337,
      itemTypeGroup: "Trims",
      itemTypeId: 15,
      itemTypeName: "TAG PIN",
      supplierId: 0,
      bomId: 1404,
      itemId: 8,
      consumptionQuantity: 4,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: 't2',
      typeSerial: 1
    },
    {
      id: 20,
      stylePartId: 115,
      subStyleId: 337,
      itemTypeGroup: "Trims",
      itemTypeId: 14,
      itemTypeName: "HANG TAG",
      supplierId: 0,
      bomId: 1403,
      itemId: 5,
      consumptionQuantity: 1,
      unitPrice: 50,
      wastagePercentage: 0,
      comments: null,
      price: 50,
      serial: 't1',
      typeSerial: 1
    },
    {
      id: 14,
      stylePartId: 115,
      subStyleId: 337,
      itemTypeGroup: "Accessories",
      itemTypeId: 19,
      itemTypeName: "SEWING THREAD",
      supplierId: 0,
      bomId: 1402,
      itemId: 6,
      consumptionQuantity: 2,
      unitPrice: 50,
      wastagePercentage: 0,
      comments: null,
      price: 100,
      serial: 'a1',
      typeSerial: 1
    },
    {
      id: 28,
      stylePartId: 114,
      subStyleId: 335,
      itemTypeGroup: "GraphicalWork",
      itemTypeId: 24,
      itemTypeName: "Karchupi",
      supplierId: 0,
      bomId: 0,
      itemId: 1,
      consumptionQuantity: 0,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: null,
      typeSerial: 0
    },
    {
      id: 25,
      stylePartId: 114,
      subStyleId: 335,
      itemTypeGroup: "GraphicalWork",
      itemTypeId: 25,
      itemTypeName: "Wash",
      supplierId: 0,
      bomId: 0,
      itemId: 1,
      consumptionQuantity: 0,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: null,
      typeSerial: 0
    },
    {
      id: 27,
      stylePartId: 114,
      subStyleId: 335,
      itemTypeGroup: "GraphicalWork",
      itemTypeId: 23,
      itemTypeName: "Embroidery",
      supplierId: 0,
      bomId: 0,
      itemId: 1,
      consumptionQuantity: 0,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: null,
      typeSerial: 0
    },
    {
      id: 26,
      stylePartId: 114,
      subStyleId: 335,
      itemTypeGroup: "GraphicalWork",
      itemTypeId: 22,
      itemTypeName: "Print",
      supplierId: 0,
      bomId: 0,
      itemId: 1,
      consumptionQuantity: 0,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: null,
      typeSerial: 0
    },
    {
      id: 31,
      stylePartId: 115,
      subStyleId: 337,
      itemTypeGroup: "GraphicalWork",
      itemTypeId: 23,
      itemTypeName: "Embroidery",
      supplierId: 0,
      bomId: 0,
      itemId: 2,
      consumptionQuantity: 0,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: null,
      typeSerial: 0
    },
    {
      id: 29,
      stylePartId: 115,
      subStyleId: 337,
      itemTypeGroup: "GraphicalWork",
      itemTypeId: 25,
      itemTypeName: "Wash",
      supplierId: 0,
      bomId: 0,
      itemId: 2,
      consumptionQuantity: 0,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: null,
      typeSerial: 0
    },
    {
      id: 30,
      stylePartId: 115,
      subStyleId: 337,
      itemTypeGroup: "GraphicalWork",
      itemTypeId: 22,
      itemTypeName: "Print",
      supplierId: 0,
      bomId: 0,
      itemId: 1,
      consumptionQuantity: 0,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: null,
      typeSerial: 0
    },
    {
      id: 32,
      stylePartId: 115,
      subStyleId: 337,
      itemTypeGroup: "GraphicalWork",
      itemTypeId: 24,
      itemTypeName: "Karchupi",
      supplierId: 0,
      bomId: 0,
      itemId: 1,
      consumptionQuantity: 0,
      unitPrice: 0,
      wastagePercentage: 0,
      comments: null,
      price: 0,
      serial: null,
      typeSerial: 0
    }
  ]
  withFullOrId = "ID";
  initialFormValues: any[] = [];

  constructor(private fb: FormBuilder) {
    this.dataForm = this.fb.group({});
  }

  ngOnInit() {
    // Initialize the form with your data
    this.initialFormValues = [...this.yourData];
    this.yourData.forEach(item => {
      this.dataForm.addControl(String(item.id), this.fb.group({
        stylePartId: [item.stylePartId],
        stylePartName: [item.stylePartName],
        subStyleId: [item.subStyleId],
        bomId: [item.bomId],
        subStyleName: [item.subStyleName],
        subStyleColor: [item.subStyleColor],
        itemTypeGroup: [item.itemTypeGroup],
        itemTypeName: [item.itemTypeName],
        itemCode: [item.itemCode],
        itemName: [item.itemName],
        consumptionQuantity: [item.consumptionQuantity],
        fabricWidth: [item.fabricWidth],
        supplierId: [item.supplierId],
        supplierName: [item.supplierName],
        measureUnitId: [item.measureUnitId],
        measureUnitName: [item.measureUnitName],
        unitPrice: [item.unitPrice],
        wastagePercentage: [item.wastagePercentage],
        comments: [item.comments],
        price: [item.price],
        id: [item.id],
        itemTypeId: [item.itemTypeId],
        itemId: [item.itemId],
        serial: [item.serial],
        typeSerial: [item.typeSerial]
      }));
    });
  }
  getFormGroupById(id: number): FormGroup {
    return this.dataForm.get(String(id)) as FormGroup;
  }


  printValue() {
    const values = this.dataForm.value;
    const valuesArray = [];
    for (const id of Object.keys(values)) {
      valuesArray.push(values[Number(id)])
    }
    const changes = this.detectChanges(this.initialFormValues, valuesArray, { "CM": ['itemTypeName', 'stylePartId'], "GraphicalWork": ['itemTypeId', 'stylePartId'], "Fabrics": ['itemTypeId', 'stylePartId', 'typeSerial'], "Accessories": ['itemTypeId', 'stylePartId', 'typeSerial'], "Trims": ['itemTypeId', 'stylePartId', 'typeSerial'] });
    console.warn(changes)
  }

  detectChanges(initialValue: any[], changesValues: any[], keyMakeGroup: any) {
    const changesArray = [];
    const indexMap1 = this.createIndexMap(initialValue, keyMakeGroup);
    const indexMap2 = this.createIndexMap(changesValues, keyMakeGroup);
    for (const key of Object.keys(indexMap1)) {
      const index1 = indexMap1[key];
      const index2 = indexMap2[key];
      if (index2 !== undefined) {
        const item1 = initialValue[index1];
        const item2 = changesValues[index2];
        const change = this.compareAndLogDifferences(item1, item2);
        if (change) {
          changesArray.push(change);
        }
      }
    }
    return changesArray;
  }

  getKeyBaseOnCondition(item: any, keyMakeGroup: any): string {
    const grpValue = keyMakeGroup[item.itemTypeGroup];
    return item.itemTypeGroup + grpValue.reduce((prev: any, curr: any) => {
      return prev + "_" + item[curr];
    }, "")
  }

  createIndexMap(list: any[], keyMakeGroup: any): { [key: string]: number } {
    const indexMap: { [key: string]: number } = {};
    list.forEach((item, index) => {
      indexMap[this.getKeyBaseOnCondition(item, keyMakeGroup)] = index;
    });
    return indexMap;
  }

  compareAndLogDifferences(item1: any, item2: any) {
    const keys = Object.keys(item1);
    const differences = keys.filter(key => item1[key] !== item2[key]);
    if (differences.length > 0) {
      let changes: any
      if (this.withFullOrId == "ID") {
        changes = { id: item1.id }
      } else if (this.withFullOrId == "FULL") {
        changes = { ...item1 }
      }
      differences.forEach(key => {
        changes["p_" + key] = item1[key];
        changes[key] = item2[key];
      });
      return changes;
    } else {
      return null;
    }
  }
}
