import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeInputTrackComponent } from './change-input-track.component';

describe('ChangeInputTrackComponent', () => {
  let component: ChangeInputTrackComponent;
  let fixture: ComponentFixture<ChangeInputTrackComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ChangeInputTrackComponent]
    });
    fixture = TestBed.createComponent(ChangeInputTrackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
