function Logger(target: Function) {
  const instance = new (target as any)();
  console.warn(`Class ${target.name} is being used. Name: ${instance.name}`);
}

@Logger
export class Person {
  name = 'raa';

  constructor() {
  }
}
