import { NgComponentOutlet } from "@angular/common";
import { Component } from "@angular/core";
import { UserComponent } from "./user/user.component";
import { ChangeInputTrackComponent } from "./change-input-track/change-input-track.component";
import { BehaviorSubjectCounterComponent } from "./behavior-subject-counter/behavior-subject-counter.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [NgComponentOutlet, ChangeInputTrackComponent,
    BehaviorSubjectCounterComponent],
  template: `<div *ngComponentOutlet="userComponent; inputs: userData"></div>
  <app-change-input-track/><br/>
  <app-behavior-subject-counter/>
 `
})
export class AppComponent {
  userComponent = UserComponent;
  userData = { name: 'Cédric', roll: 10 }
}