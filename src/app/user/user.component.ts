import { Component, Input } from "@angular/core";
import { Person } from "../custome-decorator/custom.decorator";
@Component({
  selector: 'app-user',
  standalone: true,
  template: '{{ name }} {{roll}}'
})
export class UserComponent {
  @Input({ required: true }) name!: string;
  @Input({ required: true }) roll!: number;

  constructor() {
    const p = new Person();
  }
}