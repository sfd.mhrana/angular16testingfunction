import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BehaviorSubjectCounterStateService {
  private _counter:BehaviorSubject<number> = new BehaviorSubject<number>(0);
  counter$:Observable<number> = this._counter.asObservable();

  incrementCounter():void {
    const currentValue:number = this._counter.value;
    this._counter.next(currentValue + 1);
  }

  decrementCounter():void {
    const currentValue:number = this._counter.value;
    this._counter.next(currentValue - 1);
  }
}
