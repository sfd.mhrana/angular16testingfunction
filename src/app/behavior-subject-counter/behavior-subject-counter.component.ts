import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BehaviorSubjectCounterStateService } from './behavior-subject-counter-state.service';

@Component({
  selector: 'app-behavior-subject-counter',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './behavior-subject-counter.component.html',
  styleUrls: ['./behavior-subject-counter.component.css']
})
export class BehaviorSubjectCounterComponent {
  counterValue:number = 0;

  constructor(private appStateService: BehaviorSubjectCounterStateService) {}

  ngOnInit():void {
    this.appStateService.counter$.subscribe((value:number):void => {
      this.counterValue = value;
    });
  }

  increment():void {
    this.appStateService.incrementCounter();
  }

  decrement():void {
    this.appStateService.decrementCounter();
  }
}
